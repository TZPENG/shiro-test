<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'success.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <h1>登陆成功</h1>
    <!-- shiro标签 -->
    Welcome:<shiro:principal></shiro:principal>
    
    <shiro:hasRole name="admin">
    	<br><br>
    	<a href="admin.jsp">Admin Page</a>
    </shiro:hasRole>
    
    
    <shiro:hasRole name="user">
    	<br><br>
    	<a href="user.jsp">User Page</a>
    </shiro:hasRole>
    
    <br><br>
    <a href="shiro/test">time</a>
    
    <br><br>
    <a href="shiro/logout">Logout</a>
  </body>
</html>
