package cn.tzp.shiro.service;

import java.util.Date;

import org.apache.shiro.authz.annotation.RequiresRoles;

public class ShiroService {
	
	//权限注解
	@RequiresRoles({"admin"})
	public void method() {
		System.out.println("现在时间：---->" + new Date());
	}
}
